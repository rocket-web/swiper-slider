<?php

/**
 * Fired during plugin activation
 *
 * @link       http://blog.nasrulhazim.com
 * @since      1.0.0
 *
 * @package    Swiper_Slider
 * @subpackage Swiper_Slider/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Swiper_Slider
 * @subpackage Swiper_Slider/includes
 * @author     Nasrul Hazim <nasrulhazim.m@gmail.com>
 */
class Swiper_Slider_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
