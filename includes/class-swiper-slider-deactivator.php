<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://blog.nasrulhazim.com
 * @since      1.0.0
 *
 * @package    Swiper_Slider
 * @subpackage Swiper_Slider/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Swiper_Slider
 * @subpackage Swiper_Slider/includes
 * @author     Nasrul Hazim <nasrulhazim.m@gmail.com>
 */
class Swiper_Slider_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
