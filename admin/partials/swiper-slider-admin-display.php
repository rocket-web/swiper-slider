<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://blog.nasrulhazim.com
 * @since      1.0.0
 *
 * @package    Swiper_Slider
 * @subpackage Swiper_Slider/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
