<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://blog.nasrulhazim.com
 * @since             1.0.0
 * @package           Swiper_Slider
 *
 * @wordpress-plugin
 * Plugin Name:       Swiper Slider
 * Plugin URI:        https://gitlab.com/nasrulhazim-m/swiper-slider
 * Description:       Simple Swiper Slider
 * Version:           1.0.4
 * Author:            Nasrul Hazim
 * Author URI:        http://blog.nasrulhazim.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       swiper-slider
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define('SWIPER_SLIDER_DIR', plugin_dir_path( __FILE__ ));
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-swiper-slider-activator.php
 */
function activate_swiper_slider() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-swiper-slider-activator.php';
	Swiper_Slider_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-swiper-slider-deactivator.php
 */
function deactivate_swiper_slider() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-swiper-slider-deactivator.php';
	Swiper_Slider_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_swiper_slider' );
register_deactivation_hook( __FILE__, 'deactivate_swiper_slider' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-swiper-slider.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_swiper_slider() {

	$plugin = new Swiper_Slider();
	$plugin->run();

}
run_swiper_slider();
