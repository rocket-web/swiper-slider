<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://blog.nasrulhazim.com
 * @since      1.0.0
 *
 * @package    Swiper_Slider
 * @subpackage Swiper_Slider/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Swiper_Slider
 * @subpackage Swiper_Slider/public
 * @author     Nasrul Hazim <nasrulhazim.m@gmail.com>
 */
class Swiper_Slider_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Swiper_Slider_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Swiper_Slider_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/swiper-slider-public.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-swiper', plugin_dir_url( __FILE__ ) . 'css/swiper.min.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Swiper_Slider_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Swiper_Slider_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/swiper-slider-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-swiper', plugin_dir_url( __FILE__ ) . 'js/swiper.min.js', array( 'jquery' ), $this->version, false );
	}

	public function define_shortcodes() {
		add_shortcode( 'swiper-slider', array( __CLASS__ , 'view' ) );
	}

	public function view($atts) {

		$a = shortcode_atts( array(
			'slug' => '',
			'width' => '100%',
			'height' => ''
 		), $atts );

		if(!empty($a['slug'])) {
			$args = array(
				'name'           => $a['slug'],
				'post_type'      => 'post',
				'post_status'    => 'publish',
				'posts_per_page' => 1
			);

			$post = get_posts($args);

			preg_match_all("/src=\"([^\"]*)/", $post[0]->post_content, $images);

			$images = $images[1];

			require_once 'partials/view.php';
		}
		// display nothing when no slug given
	}

}
